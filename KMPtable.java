//Adam Williams 1518245.
//Joseph Smith 1578087.

import java.lang.*;
import java.util.*;
import java.io.*;

public class KMPtable{
    public static void main(String[] args){

        try{
            String pattern = args[0];

            String compare = pattern;
            ArrayList<Character> alphabet = new ArrayList<Character>(); //has the individual characters
            ArrayList<String> resultList = new ArrayList<String>();
            char[] charArray = new char[pattern.length()];//contains the list in a character array
            String pattern2 = "   ";
            int counter = 0; // counter for any other letter
            String anyOtherLetter = "*  "; 
            
            //this for loop initialises the arrays
            for(int i = 0; i < pattern.length(); i++){
                charArray[i] = pattern.charAt(i);
                
                counter++;
                anyOtherLetter += counter + "  ";

                if(!alphabet.contains(charArray[i])){
                    alphabet.add(charArray[i]);
                }
            }

            for(Character c : pattern.toCharArray()){
                pattern2 += c + "  ";
            }
            resultList.add(pattern2);
            String dictionary = "";
            String compareString = "";
            String result = "";
            String smallDicitionary = "";
            String smallCompare = "";
            int shiftCounter = 0;
            
            //Goes through every alphabet letter.
            for(int i = 0; i < alphabet.size(); i++){ // 0 = x ,1 =  y , 2 = z 
                char character = alphabet.get(i);
                dictionary = "";
                result = "";
                result += character + "  "; // sets up the start of the table to have a letter from the pattern in first coloumn
                //Goes through every character in the pattern.
                for(int j = 0; j < charArray.length; j++){ //0 = x, 1 = y, 2 = x, 3= y, 4 = z
                    dictionary += charArray[j];   // could be x,xy,xyx,xyxy,xyxyz

                    compareString = dictionary.substring(0,dictionary.length()-1) + character; //example of input would be XX, XYX
                    if(compareString.equals(dictionary)){ //if they are the same 0 shifts
                        result += "0  ";   
                    }else{
                        smallDicitionary = dictionary.substring(0, dictionary.length() - 1); //turns xy into x
                        smallCompare = compareString.substring(1, compareString.length()); //turns xy into y
                        shiftCounter++;
                        if(smallDicitionary.length() == 0){ 
                            result += shiftCounter + "  ";
                            shiftCounter = 0;
                        }else{
                            //If length is greater than zero 
                            while(smallCompare.length() > 0){
                                if(smallDicitionary.equals(smallCompare)){
                                    result += shiftCounter + "  ";
                                    shiftCounter = 0;
                                    break;
                                }else{
                                    smallDicitionary = smallDicitionary.substring(0, smallDicitionary.length() - 1);
                                    smallCompare = smallCompare.substring(1, smallCompare.length());
                                    shiftCounter++;
                                    if(smallDicitionary.length() == 0){
                                        result += shiftCounter + "  ";
                                        shiftCounter = 0;
                                    }
                                }
                            }
                        }
                    }
                    
                }
                resultList.add(result + " ");
                
            }
            resultList.add(anyOtherLetter);

            File skipArrayFile = new File(pattern + ".kmp");
            
            BufferedWriter ow = null;
            ow = new BufferedWriter(new FileWriter(pattern + ".kmp"));

            for(int i = 0; i<resultList.size(); i++){
                ow.write(resultList.get(i));
                ow.newLine();
            }
            ow.flush();
            ow.close();
            //strictly used for printing
            
            

        }catch(Exception e){
            
        }
    }
}