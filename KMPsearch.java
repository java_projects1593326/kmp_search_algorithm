//Adam Williams 1518245.
//Joseph Smith 1578087.

import java.lang.*;
import java.util.*;
import java.io.*;


class KMPsearch{

    public static void main(String[] args){

        try{

            String skipArrayName = args[0];
            String fileToSearch = args[1];
            ArrayList<String> skipArrayList = new ArrayList<String>();
            ArrayList<Character> alphabet = new ArrayList<Character>();
            ArrayList<Character> charLine = new ArrayList<Character>();

            File skipArray = new File(skipArrayName);

            Scanner reader = new Scanner(skipArray);
            BufferedReader br = new BufferedReader(new FileReader(fileToSearch));

            while(reader.hasNextLine()){
                String data = reader.nextLine();
                String newData = data.replaceAll("\\s","");
                skipArrayList.add(newData);

            }

            String firstLine = skipArrayList.get(0);

            char[] pattern = new char[firstLine.length()];

            //Makes alphabet
            for(int i = 0; i < firstLine.length(); i++){
                pattern[i] = firstLine.charAt(i);

                if(!alphabet.contains(pattern[i])){
                    alphabet.add(pattern[i]);
                }
            }

            String st2 = "";            
            int counter = 0;
            int index = 0;
            int lineNum = 0;
            

            skipArrayList.remove(0);

            //Reads in line from test.txt
            while((st2 = br.readLine()) !=null){
    
                counter = 0;
                lineNum ++;

                charLine.removeAll(charLine);
                //Turns line read into char array list
                for(int k = 0; k < st2.length(); k++){
                    charLine.add(st2.charAt(k));
                }

                //Loops through char arraylist
                for(int i = 0; i < charLine.size(); i++){
                    
                    //if character from text is in our pattern alphabet then finds index of array from the table
                    //gerated from kmptable.
                    if(alphabet.contains(charLine.get(i))){
                        for(String s: skipArrayList){
                            if(s.contains(Character.toString(charLine.get(i)))){
                                index = skipArrayList.indexOf(s);
                                break;
                            }
                        }
                        //Saves table row with skip values.
                        String rowIndex = skipArrayList.get(index);
                        
                        //Gets the number of skips from the table. 
                        int num = Character.getNumericValue(rowIndex.charAt(counter + 1));
                        

                        //If we have been through the length of the pattern and num of skips 
                        //equals zero then print line num and index of where pattern was found.
                        if(counter == (firstLine.length() - 1) && num == 0){
                            int index2 = i - pattern.length;
                            System.out.println("PATTERN LINE: " + Integer.toString(lineNum) + " PATTERN INDEX: " + Integer.toString(index2));
                            counter = 0;
                            break;
                        }
                        //Skips through array by number found in table.
                        else{
                            i += num;
                            counter ++;
                        }
                        
                    }
                    //For anyother character get the amount of skips from KMPtable.
                    else{
                        int skip = 0;

                        String arrayLine = skipArrayList.get(skipArrayList.size() - 1);

                        skip = Character.getNumericValue(arrayLine.charAt(counter + 1));
                        i += skip - 1;
                        counter = 0;
                    }
                }

            }
        }
        catch(Exception e){
            System.err.println(e);
        }
    }
}